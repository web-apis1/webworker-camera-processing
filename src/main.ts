import { Action } from './common'
import './style.css'

// CONFIG
const PROCESSING_DELAY = 500 // ms

// UI
const rootElem = document.getElementById('app') as HTMLDivElement

const videoElem = document.createElement('video')
videoElem.classList.add('camera-video')

const startCameraElem = document.createElement('button')
startCameraElem.textContent = 'Start Camera'
startCameraElem.addEventListener('click', () => { startCamera(videoElem) })

const processingResultElem = document.createElement('div')

rootElem.append(processingResultElem, videoElem, startCameraElem)

// CAMERA
async function startCamera(startCameraElem: HTMLVideoElement) {
  try {
    const cameraStream = await navigator.mediaDevices.getUserMedia({video: true})
    startCameraElem.srcObject = cameraStream
    startCameraElem.play()

    processMediaStream()
  }
  catch(e) {
    console.error(`Error: ${(e as Error).message}`)
  }
}

// CAMERA STREAM PROCESSING (WEBWORKER)
async function processMediaStream() {
  const worker = new Worker(new URL('./ww_stream_processing.ts', import.meta.url), { type: 'module' })

  worker.addEventListener('message', <T>(event: MessageEvent<Action<T>>) => { 
    const {type, payload} = event.data

    if(type == 'MODEL_LOADED') { processFrame(worker) }

    else if(type === 'PROCESSING_RESULT') { 
      const processingResult = payload as string
      processingResultElem.textContent = processingResult

      setTimeout(() => processFrame(worker), PROCESSING_DELAY)
    }
  })

  const loadModelAction: Action<undefined> = { type: 'LOAD_MODEL' } 
  worker.postMessage(loadModelAction)  
}

function processFrame(worker: Worker) {
  videoElem.requestVideoFrameCallback(() => {
      const videoFrame = new VideoFrame(videoElem) // ???? critical part ????

      const processFrameAction: Action<VideoFrame> = { type: 'PROCESS_FRAME', payload: videoFrame } 
      worker.postMessage(processFrameAction, [ processFrameAction.payload! ]) // videoframe is transfarable (=> no ser/de required)
  })
}
