import '@tensorflow/tfjs-backend-webgl' // CURRENT TENSORFLOW BACKEND

import { browser } from '@tensorflow/tfjs'
import { ObjectDetection, load as loadDetectionModel } from '@tensorflow-models/coco-ssd'
import { Action } from './common'

// PROCESSING CONFIG
const PREDICTION_TRESHOLD = 0.65


let detector: ObjectDetection | undefined

self.addEventListener('message', async <T>(event: MessageEvent<Action<T>>) => {
    const {type, payload} = event.data

    if(type === 'LOAD_MODEL') { 
        detector = await loadDetectionModel()
        const modelLoadedAction: Action<undefined> = { type: 'MODEL_LOADED' }
        console.log(detector)
        self.postMessage(modelLoadedAction)
    }

    else if(type === 'PROCESS_FRAME' && detector) {
        const videoFrame = payload as VideoFrame
        const tensors = browser.fromPixels(await createImageBitmap(videoFrame))

        const predictions = await detector.detect(tensors)
        const prediction = predictions.find(p => p.score >= PREDICTION_TRESHOLD)
        
        tensors.dispose()  // mem leak fix
        videoFrame.close() // fix 'videoframe was GC without being closed'
        
        const resultAction: Action<string> = { type: 'PROCESSING_RESULT', payload: prediction?.class ?? "NOTHING_DETECTED" }
        self.postMessage(resultAction)
    }
})


